<?php

namespace Drupal\pixelsize\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'PixelsizeDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pixelsize_default_formatter",
 *   label = @Translation("Pixelsize Formatter"),
 *   field_types = {
 *     "pixelsize"
 *   }
 * )
 */
class PixelsizeDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->width . $this->t('×') . $item->height . $this->t('px'),
      ];
    }

    return $elements;
  }

}
