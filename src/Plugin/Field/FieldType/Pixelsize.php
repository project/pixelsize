<?php

namespace Drupal\pixelsize\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "pixelsize",
 *   label = @Translation("Pixelsize"),
 *   description = @Translation("Stores a Pixel Size"),
 *   category = @Translation("Custom"),
 *   default_widget = "pixelsize_default_widget",
 *   default_formatter = "pixelsize_default_formatter"
 * )
 */
class Pixelsize extends FieldItemBase {

  /**
   * Field type properties definition.
   *
   * Inside this method we defines all the fields (properties) that our
   * custom field type will have.
   */
  public static function propertyDefinitions(StorageDefinition $storage) {
    $properties = [];
    $properties['width'] = DataDefinition::create('integer')->setLabel(t('Width'));
    $properties['height'] = DataDefinition::create('integer')->setLabel(t('Height'));
    return $properties;
  }

  /**
   * Field type schema definition.
   *
   * Inside this method we defines the database schema used to store data for
   * our field type.
   *
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {
    $columns = [];
    $columns['width'] = [
      'type' => 'int',
      'unsigned' => TRUE,
    ];
    $columns['height'] = [
      'type' => 'int',
      'unsigned' => TRUE,
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty.
   *
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */
  public function isEmpty() {
    $isEmpty =
      empty($this->get('width')->getValue()) &&
      empty($this->get('height')->getValue());

    return FALSE;
  }

}
