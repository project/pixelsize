<?php

namespace Drupal\pixelsize\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'PixelsizeDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "pixelsize_default_widget",
 *   label = @Translation("Pixelsize Widget"),
 *   field_types = {
 *     "pixelsize"
 *   }
 * )
 */
class PixelsizeDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   *
   * Inside this method we can define the form used to edit the field type.
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    Array $element,
    Array &$form,
    FormStateInterface $formState
  ) {

    // Width.
    $element['width'] = [
      '#type' => 'number',
      '#title' => t('Width'),
      '#min' => 0,
      '#step' => 1,
      '#size' => 3,
      '#field_suffix' => $this->t('px'),
      '#default_value' => isset($items[$delta]->width) ? $items[$delta]->width : NULL,
      '#empty_value' => '',
      '#attached' => [
        'library' => 'pixelsize/widget',
      ],
    ];

    $element['by'] = [
      '#type' => 'item',
      '#markup' => $this->t('×'),
    ];

    // Height.
    $element['height'] = [
      '#type' => 'number',
      '#title' => t('Height'),
      '#min' => 0,
      '#step' => 1,
      '#size' => 3,
      '#field_suffix' => $this->t('px'),
      '#default_value' => isset($items[$delta]->height) ? $items[$delta]->height : NULL,
      '#empty_value' => '',
    ];

    return $element;
  }

}
